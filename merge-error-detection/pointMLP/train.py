import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor

from utils import Center, Normalization
from dataloader import PointCloudModule
from module import PointMLPModule

# Set up callbacks
lr_monitor = LearningRateMonitor(logging_interval='step')

# Define datamodule specific parameters (config)
BS = 9
SCALE_NORM = 5000
TRANSFORM = {
    'train': [Center(), Normalization(SCALE_NORM)],
    'val': [Center(), Normalization(SCALE_NORM)],
    'test': [Center(), Normalization(SCALE_NORM)],
}

# Only this needed for training dataloader actually
ROOT = f'/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only/'

LIMIT_NUM_SAMPLES = None
NUM_WORKERS = 8
SHUFFLE = True

datamodule = PointCloudModule(batch_size = BS, transforms = TRANSFORM, root = ROOT, num_workers = NUM_WORKERS, 
                                shuffle=SHUFFLE, limit_num_samples = LIMIT_NUM_SAMPLES)

train_loader = datamodule.train_dataloader()
val_loader = datamodule.val_dataloader()

# Define model

hyperparameters = {
    'lr' : 1e-3,
    'num_classes' : 2,
    'step_size': 10}
    # 'points':2048,
#     'embed_dim':64, 
#     'groups':1, 
#     'res_expansion':1.0,
#     'activation':"relu", 
#     'bias':True, 
#     'use_xyz':True, 
#     'normalize':"center",
#     'dim_expansion':[2, 2, 2, 2, 2], 
#     'pre_block':[2, 2, 2, 2, 2], 
#     'pos_blocks':[2, 2, 2, 2, 2],
#     'k_neighbors':[32, 32, 32, 32], 
#     'reducers':[2, 2, 2, 2],
# }

model = PointMLPModule(num_cls=2, **hyperparameters)

# CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run0/checkpoints/epoch=22-step=63871.ckpt'
CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_reg_run0/checkpoints/epoch=22-step=63871.ckpt'
# CHKPT_PATH = None

if CHKPT_PATH is not None:
    model = model.load_from_checkpoint(CHKPT_PATH)

# Define triaing parameters
NUM_EPOCHS = 150
ACCELERATOR = "gpu"
DEVICES = "auto"

# Define monitors and callbacks
from pytorch_lightning.callbacks import ModelCheckpoint
checkpoint_callback = ModelCheckpoint(
            filename='{epoch}-{val_loss:.2f}-{other_metric:.2f}',
                monitor= 'epoch/val_loss',
                    save_top_k = 5)

from pytorch_lightning.callbacks.early_stopping import EarlyStopping
early_stop_callback = EarlyStopping(monitor="epoch/val_loss", min_delta=0.00, patience=5, verbose=False, mode="min")


# Some general info about the training before starting it
print("Len training dataset : ", len(datamodule.train_dataset),
    "Batch Size : ", BS, "NUM_EPOCHS : ",NUM_EPOCHS )
print("Total training steps : ", len(datamodule.train_dataset)//BS*NUM_EPOCHS)


# Define Trainer and start the training 
trainer = pl.Trainer(max_epochs=NUM_EPOCHS,
                    accelerator=ACCELERATOR, #devices=DEVICES,
                    # auto_scale_batch_size=True,
                    callbacks=[lr_monitor],
                    log_every_n_steps=2,
                    # strategy=DDPStrategy(find_unused_parameters=False)
                    )

trainer.fit(model, train_dataloaders=train_loader, val_dataloaders=val_loader)