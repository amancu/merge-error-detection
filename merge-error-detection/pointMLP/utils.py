import numpy as np
import openmesh
import open3d as o3d
import torch
import morphx.processing.clouds as clouds

from syconn.reps.super_segmentation import SuperSegmentationObject, SuperSegmentationDataset
from syconn.reps.segmentation import SegmentationDataset
from syconn import global_params
# from syconn.handler.prediction_pts import sso2hc
from typing import Optional, List, Union, Callable, Tuple
from scipy.spatial import cKDTree
from sklearn.preprocessing import label_binarize
from morphx.classes.hybridmesh import HybridCloud
from morphx.classes.pointcloud import PointCloud
from morphx.processing.hybrids import extract_subset
from morphx.processing.objects import context_splitting_graph_many, context_splitting_kdt

pts_feat_dict = dict(sv=0, mi=1, syn_ssv=3, syn_ssv_sym=3, syn_ssv_asym=4, vc=2, sv_myelin=5)
# in nm, should be replaced by Poisson disk sampling
pts_feat_ds_dict = dict(merger=dict(sv=70))

def mesh2obj_file_colors(dest_path: str, mesh: List[np.ndarray],
                  colors: Optional[Union[int, np.ndarray]] = None,
                  center: Optional[np.ndarray] = None,
                  scale: Optional[float] = None):
    """
    Writes mesh to .obj file.

    Args:
        dest_path: Path to file.
        mesh: Flat arrays of indices (triangle faces), vertices and normals.
        colors: List of numpy arrays (rgba) for the colors of vertices, must contain the same length as the vertices.
        center: Subtracts center from original vertex locations.
        scale: Multiplies vertex locations after centering.

    Returns:

    """
    mesh_obj = openmesh.TriMesh()
    ind, vert, norm = mesh
    if vert.ndim == 1:
        vert = vert.reshape(-1, 3)
    if ind.ndim == 1:
        ind = ind.reshape(-1, 3)
    if center is not None:
        vert -= center
    if scale is not None:
        vert *= scale
    vert_openmesh = []
    if colors is not None:
        mesh_obj.request_vertex_colors()
        colors = colors.astype(np.float64)
    for i, v in enumerate(vert):
        v = v.astype(np.float64)  # Point requires double
        v_openmesh = mesh_obj.add_vertex(v)
        if colors is not None:
            mesh_obj.set_color(v_openmesh, colors[i])
        vert_openmesh.append(v_openmesh)
    for f in ind:
        f_openmesh = [vert_openmesh[f[0]], vert_openmesh[f[1]],
                      vert_openmesh[f[2]]]
        mesh_obj.add_face(f_openmesh)

    openmesh.write_mesh(dest_path, mesh_obj, vertex_color=True)


def load_hc_pkl(path: str, gt_type: str, radius: Optional[float] = None) -> HybridCloud:
    """
    Load HybridCloud from pkl file (cached via functools.lur_cache).
    The following properties must be met:

    * Vertex features are labeled according to ``pts_feat_dict`` in
      handler.prediction_pts.
    * Skeleton nodes require to have labels (0, 1) indicating whether they can
      be used as source node for the context generation (1) or not (0).

    Down sampling will be performed via open3d's ``voxel_down_sample`` with
    voxel sizes defined in pts_feat_ds_dict (handler.prediction_pts)

    Args:
        path: Path to HybridCloud pickle file.
        gt_type: See pts_feat_ds_dict in handler.prediction_pts.
        radius: Add additional edges between skeleton nodes within `radius`.

    Returns:
        Populated HybridCloud.
    """
    feat_ds_dict = dict(pts_feat_ds_dict[gt_type])
    # requires cloud keys to be in [vc, syn_ssv, mi, hybrid]
    feat_ds_dict['hybrid'] = feat_ds_dict['sv']
    hc = HybridCloud()
    hc.load_from_pkl(path)
    new_verts = []
    new_labels = []
    new_feats = []
    for ident_str, feat_id in pts_feat_dict.items():
        pcd = o3d.geometry.PointCloud()
        m = (hc.features == feat_id).squeeze()
        # print(f'm for {ident_str}: {m}')
        if np.sum(m) == 0:
            if not (feat_id == 0 and len(hc.features) == 0 and len(hc.vertices) != 0):
                continue
            m = np.ones(shape=(len(hc.vertices),), dtype=np.bool)

        verts = hc.vertices[m]          # (N,3)
        labels = hc.labels[m]
        feats = hc.features[m]
        pcd.points = o3d.utility.Vector3dVector(verts)
        pcd, idcs, int_vector = pcd.voxel_down_sample_and_trace(
            pts_feat_ds_dict[gt_type][ident_str], pcd.get_min_bound(),
            pcd.get_max_bound())
        idcs = np.max(idcs, axis=1)
        new_verts.append(np.asarray(pcd.points))
        new_labels.append(labels[idcs])
        new_feats.append(feats[idcs])
    hc._vertices = np.concatenate(new_verts)
    hc._labels = np.concatenate(new_labels)
    hc._features = np.concatenate(new_feats)
    # reset verts2node mapping and cache it
    hc._verts2node = None
    _ = hc.verts2node
    if radius is not None:
        # add edges within radius
        kdt = cKDTree(hc.nodes)
        pairs = list(kdt.query_pairs(radius))
        # remap to subset of indices
        hc._edges = np.concatenate([hc._edges, pairs])
    return hc


def sso2hc(sso: SuperSegmentationObject, feats: Union[Tuple, str], feat_labels: Union[Tuple, int], pt_type: str,
           radius: int = None, label_remove: List[int] = None, label_mappings: List[Tuple[int, int]] = None):
    if type(feats) == str:
        feats = [feats]
    if type(feat_labels) == int:
        feat_labels = [feat_labels]
    vert_dc = dict()
    obj_bounds = {}
    offset = 0
    idcs_dict = {}
    for k in feats:
        pcd = o3d.geometry.PointCloud()
        verts = sso.load_mesh(k)[1].reshape(-1, 3)
        pcd.points = o3d.utility.Vector3dVector(verts)
        pcd, idcs, _ = pcd.voxel_down_sample_and_trace(pts_feat_ds_dict[pt_type][k], pcd.get_min_bound(),
                                                    pcd.get_max_bound())
        idcs = np.max(idcs, axis=1)
        idcs_dict[k] = idcs
        vert_dc[k] = np.asarray(pcd.points)
        obj_bounds[k] = [offset, offset+len(pcd.points)]
        offset += len(pcd.points)
    sample_feats = np.concatenate([[feat_labels[ii]] * len(vert_dc[k])
                                   for ii, k in enumerate(feats)]).reshape(-1, 1)
    sample_pts = np.concatenate([vert_dc[k] for k in feats])
    if not sso.load_skeleton():
        raise ValueError(f'Couldnt find skeleton of {sso}')
    nodes, edges = sso.skeleton['nodes'] * sso.scaling, sso.skeleton['edges']
    hc = HybridCloud(nodes, edges, vertices=sample_pts, features=sample_feats, obj_bounds=obj_bounds)

    if label_remove is not None:
        hc.remove_nodes(label_remove)
    if label_mappings is not None:
        hc.map_labels(label_mappings)
    # cache verts2node
    _ = hc.verts2node
    if radius is not None:
        # add edges within radius
        kdt = cKDTree(hc.nodes)
        pairs = list(kdt.query_pairs(radius))
        # remap to subset of indices
        hc._edges = np.concatenate([hc._edges, pairs])
    return hc, idcs_dict

def pts_loader_semseg_train_mlp(fname_pkl: str, batchsize: int,
                            npoints: int, ctx_size: float,
                            transform: Optional[Callable] = None,
                            use_subcell: bool = False,
                            gt_type: str = 'merger',
                            ) -> Tuple[np.ndarray, Tuple[np.ndarray, np.ndarray]]:


    feat_dc = dict(pts_feat_dict)
    del feat_dc['syn_ssv_asym']
    del feat_dc['syn_ssv_sym']
    del feat_dc['sv_myelin']
    if not use_subcell:
        del feat_dc['mi']
        del feat_dc['vc']
        del feat_dc['syn_ssv']

    # in 1 out 5 events augment the context size by factor [0.6, 1.4] drawn from a normal distribution with mean 1
    # and std 0.1
    # if np.random.randint(0, 4) == 0:
    #     fluct = 1
    # else:
    #     fluct = min(max(np.random.randn(1)[0] * 0.1 + 1, 0.8), 1.2)
    ctx_size_fluct = ctx_size

    hc = load_hc_pkl(fname_pkl, gt_type)
    reg_target = hc.predictions['1']
    # print(f'hc labels: {np.unique(hc.labels)}')
    # filter valid skeleton nodes (i.e. which were close to manually annotated nodes)
    # use representative coordinate to gather source nodes
    # Get source nodes starting from the representative coordinate
    source_nodes = np.where(hc.node_labels > 0)[0]
    
    source_nodes = np.random.choice(source_nodes, batchsize, replace=len(source_nodes) < batchsize)
    source_node_labels = hc.node_labels[source_nodes]

    hc_subs = []
    # create contexts for batch
    for source_node in source_nodes:
        # create local context
        while True:
            if hc.node_labels[source_node] not in source_node_labels:
                raise ValueError(f'Invalid source node in "{fname_pkl}".')
            node_ids = context_splitting_graph_many(hc, [source_node], ctx_size_fluct)[0]
            hc_sub = extract_subset(hc, node_ids)[0]  # only pass HybridCloud
            sample_feats = hc_sub.features
            if len(sample_feats) > 0:
                break
            source_node = np.random.choice(source_nodes)
        hc_subs.append(hc_sub)


    # create batches to return
    npoints_ssv =  npoints#min(len(hc.vertices), npoints)
    if npoints_ssv == 0:
        raise ValueError(f'No vertices in "{fname_pkl}".')

    n_out_pts_curr = npoints_ssv
    batch = np.zeros((npoints_ssv, 3))                                   # TODO change here
    batch_f = np.ones((npoints_ssv, len(feat_dc)))

    cnt = 0

    # process contexts to
    for i, source_node in enumerate(source_nodes):
        # get current context and process it
        hc_sub = hc_subs[i]

        sample_pts = hc_sub.vertices
        sample_labels = hc_sub.labels
        sample_feats = hc_sub.features

        # Check for too many points inside context
        while len(sample_pts) != n_out_pts_curr:
            if len(sample_pts) > n_out_pts_curr:
                pcd = o3d.geometry.PointCloud()
                pcd.points = o3d.utility.Vector3dVector(sample_pts)
                pcd, idcs, int_vectors = pcd.voxel_down_sample_and_trace(500, pcd.get_min_bound(), pcd.get_max_bound())
                base_points = np.max(idcs, axis=1)
                base_points = np.random.choice(base_points, n_out_pts_curr,
                                                replace=len(base_points) < n_out_pts_curr)
                sample_pts = hc_sub.vertices[base_points]
                sample_labels = hc_sub.labels[base_points]
                sample_feats = hc_sub.features[base_points]
            # Check for too few points inside context
            elif len(sample_pts) < n_out_pts_curr:
                npoints_add = n_out_pts_curr - len(sample_pts)
                idx = np.random.choice(len(sample_pts), npoints_add)
                sample_pts = np.concatenate([sample_pts, sample_pts[idx]])
                sample_feats = np.concatenate([sample_feats, sample_feats[idx]])
                sample_labels = np.concatenate([sample_labels, sample_labels[idx]])

        # TODO CHANGE HERE FOR UPDATED FEATURES
        # sample_feats = label_binarize(sample_feats, classes=np.arange(len(feat_dc)))
        sample_feats = np.zeros(shape=(len(sample_pts,)),dtype = np.int32)
        hc_sub._vertices = sample_pts
        hc_sub._features = sample_feats
        hc_sub._labels = sample_labels
        init_reg = reg_target
        # apply augmentations
        if transform is not None:
            reg_target = transform[0](hc_sub, reg_target)
            reg_target = transform[1](hc_sub, reg_target)
        batch = hc_sub.vertices
        batch_f = hc_sub.features
        cnt += 1
    assert cnt == batchsize

    return hc, batch, batch_f, reg_target, init_reg

def pts_loader_semseg_infer_mlp(fname_pkl: Union[str,int], redundancy: int = 32,
                            npoints: int = 20000, ctx_size: float = 20000, ctx_dst_fac: int = 3,
                            transform: Optional[Callable] = None,
                            use_subcell: bool = False,
                            gt_type: str = 'merger', ssd: Optional[SuperSegmentationDataset] = None,
                            ) -> Tuple[np.ndarray, Tuple[np.ndarray, np.ndarray]]:


    feat_dc = dict(pts_feat_dict)
    del feat_dc['syn_ssv_asym']
    del feat_dc['syn_ssv_sym']
    del feat_dc['sv_myelin']
    if not use_subcell:
        del feat_dc['mi']
        del feat_dc['vc']
        del feat_dc['syn_ssv']

    # Load from pickled artificial clouds or create cloud from SSV id
    if isinstance(fname_pkl,int):
        if ssd is None:
            raise NotImplementedError("Loading cells from dataset requires a valid SSD")
        sso = ssd.get_super_segmentation_object(fname_pkl)
        hc, voxel_dict = sso2hc(sso, 'sv', 0, gt_type)
        sso_size = sso.size
        # Fix size parameter in case of overflow
        if sso_size < 0:
            sd = SegmentationDataset('sv', working_dir=global_params.config.working_dir)
            sv = sd.get_segmentation_object(sso.id)
            sv.load_attr_dict()
            sso_size = sv.attr_dict["size"]

    else:
        hc = load_hc_pkl(fname_pkl, gt_type)
        sso_size = 0
    
    if not isinstance(fname_pkl,int):
        init_reg = hc.predictions['1']
    else: 
        init_reg = np.array([0,0,0], dtype=np.float32)
    # else:
    #     hc = HybridCloud()

    # choose base nodes with context overlap
    base_node_dst = ctx_size / ctx_dst_fac
    bs = 1

    # select source nodes for context extraction
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(hc.nodes)
    pcd, idcs, _ = pcd.voxel_down_sample_and_trace(base_node_dst, pcd.get_min_bound(), pcd.get_max_bound())
    source_nodes = np.max(idcs, axis=1)

    # create batch number
    n_batches = int(np.ceil(len(source_nodes) / bs))
    node_arrs = context_splitting_kdt(hc, source_nodes, ctx_size)

    # initialize list of data
    batch_v = np.zeros((n_batches*bs, npoints, 3))
    batch_f = np.zeros((n_batches*bs, npoints))
    mask = np.zeros((n_batches*bs, npoints), dtype=bool)
    batch_sn = np.zeros((n_batches*bs,1))
    # print(f'batch v shape {batch_v.shape}')
    # used later for removing cell organelles
    idcs_list = []

    # generate contexts
    cnt = 0
    for ii, node_arr in enumerate(node_arrs):
        hc_sub, idcs_sub = extract_subset(hc, node_arr)
        ix = 0
        while len(hc_sub.vertices) == 0:
            if ix >= len(hc.nodes):
                raise IndexError(f'Could not find suitable context in HC during "extract_subhcs".')
            elif ix >= len(node_arrs):
                # if the cell fragment, represented by hc, is small and its skeleton not well centered,
                # it can happen that all extracted sub-skeletons do not contain any vertex. in that case
                # use any node of the skeleton
                # print(f'in radnom')
                sn = np.random.randint(0, len(hc.nodes))
                hc_sub, idcs_sub = extract_subset(hc, context_splitting_kdt(hc, sn, ctx_size))
            else:
                hc_sub, idcs_sub = extract_subset(hc, node_arrs[ix])
            ix += 1
        # fill batches with sampled and transformed subsets
        hc_sample, idcs_sample = clouds.sample_cloud(hc_sub, npoints)

        # Create inner context mask
        # tree = cKDTree(data=hc_sample.vertices)
        # inner_ids = tree.query_ball_point(hc.nodes[source_nodes[ii]], r=15000)
        # mask = np.ones(shape=(len(hc_sample.vertices),), dtype=bool)
        # mask[inner_ids] = bool(0)
        # inner_mask = np.zeros(shape=(len(hc_sample.vertices),), dtype=bool)
        # inner_mask[inner_ids] = bool(1)
        # # get vertex indices respective to total hc
        # global_idcs = idcs_sub[idcs_sample.astype(int)]
        # global_idcs = global_idcs[inner_mask]             uncomment for inner focus of context

        if transform is not None:
            reg_target = transform[0](hc_sample, init_reg)      # Center
            reg_target = transform[1](hc_sample, init_reg)      # Normalization
        batch_v[cnt] = hc_sample.vertices
        batch_f[cnt] = hc_sample.features.squeeze()
        # masks get used later when mapping predictions back onto the cell surface during postprocessing
        batch_sn[cnt] = source_nodes[ii]
        cnt = cnt + 1
        # print(f'cnt: {cnt}')

    return hc, batch_v, batch_f, batch_sn, reg_target, init_reg, sso_size



################################### My own Point Cloud Tranformations ###################################
class Transformation:
    @property
    def augmentation(self):
        return None

    @property
    def attributes(self):
        return None


class Center(Transformation):
    center_loc = None

    def __init__(self, center_loc: Optional[Union[float, np.ndarray]] = None, distr: str = 'const'):
        """

        Args:
            center_loc: Location of center.
            distr: Distribution used. If 'const': Center_loc will be used as is. If scalar used for every dimension.
                If 'uniform' sample randomly between zero and `center_loc`, all dimensions of `center_loc` will be
                multiplied with independently drawn values between 0 and 1.
        """
        if center_loc is None:
            center_loc = np.zeros(3)
        elif np.isscalar(center_loc):
            center_loc = [center_loc] * 3
        self.center_loc = np.array(center_loc).squeeze()
        self.distr = distr

    def __call__(self, pc: PointCloud, merge_coord: np.ndarray):
        """ Centers the given PointCloud only with respect to vertices. If the PointCloud is an HybridCloud, the nodes
            get centered as well but are not taken into account for centroid calculation. Operates in-place for the
            given PointCloud
        """
        centroid = np.mean(pc.vertices, axis=0)
        if self.distr == 'const':
            offset = self.center_loc
        elif self.distr == 'uniform':
            offset = ((np.random.random(min(1, len(self.center_loc))) - 0.5) * 2 * self.center_loc).squeeze()
        else:
            raise ValueError(f'Given distr value is not implemented.')
        move_vec = (-centroid + offset)
        # Transform the point cloud
        pc.move(move_vec)
        return merge_coord + move_vec

    @property
    def augmentation(self):
        return False


class Normalization(Transformation):
    def __init__(self, radius: Optional[int]):
        if radius is not None:
            if radius <= 0:
                radius = 1
            self._radius = -radius
        else:
            self._radius = radius

    def __call__(self, pc: PointCloud, merge_coord: np.ndarray):
        """ Divides the coordinates of the points by the context size (e.g. radius of the local BFS). If radius is not
            valid (<= 0) it gets set to 1, so that the normalization has no effect.
        """
        pc.scale(self._radius)
        factor = np.array([self._radius] * 3)
        res = merge_coord / -factor
        return res

    @property
    def radius(self):
        return self._radius

    @property
    def augmentation(self):
        return False

    @property
    def attributes(self):
        return self._radius

################################### Measures ###################################

def nmse(pred, ref, axes = (-1)):
    """ Compute the normalized mean squared error (nmse)
    :param pred: input regression prediction (torch.Tensor)
    :param ref: reference regression target (torch.Tensor)
    :param axes: tuple of axes over which the nmse is computed
    :return: (mean) nmse (np.array)
    """
    nominator = torch.pow((pred - ref),2).sum(dim=axes)
    denominator = torch.pow((ref),2).sum(dim=axes)
    # print(f'nom {nominator}\tdenom: {denominator}')
    nmse = (nominator / denominator)
    return nmse.mean().detach().cpu().numpy()

def regression_accuracy(pred, ref, thresh=0.1, axes = (-1)):
    """ Compute the accuracy in a regression task within a specific threshold
    :param pred: input regression prediction (torch.Tensor) shape: (B,N,3) or (N,3)
    :param ref: reference regression target (torch.Tensor) shape: (B,N,3) or (N,3)
    :param threshold: threshold for a valid regression (also seen as vicinity)
    :return: accuracy (np.array)
    """
    pred, ref = pred.detach().cpu().numpy(), ref.detach().cpu().numpy()
    dist = np.linalg.norm((pred - ref), axis=axes)
    # print(f'dist: {dist}')
    res = np.where(dist <= thresh, True, False)
    res = np.count_nonzero(res)
    if len(pred.shape) == 3: 
        total = pred.shape[1]
    else: 
        total = pred.shape[0]
    res = res/total
    return res

# For testing purposes only
if __name__ == "__main__":
    a= torch.Tensor([[[0.,0.,0.],[0.,0.,0.],[0.,0.,0.]],[[0.,0.,0.],[0.,0.,0.],[0.,0.,0.]]])
    b= torch.Tensor([[[0.01,0.01,0.01],[0.,0.,0.],[0.,0.,0.]],[[0.01,0.01,0.01],[0.,0.,0.],[0.,0.,0.]]])
    print(f'a shape: {a.shape}')
    regression_accuracy(a,b)
