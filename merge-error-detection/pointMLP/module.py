import torch
import pytorch_lightning as pl
import numpy as np

from torch import nn
from torch.autograd import Variable
from model import pointMLPElite
# from medutils.measures import mse
from utils import mesh2obj_file_colors, nmse, regression_accuracy
from torchmetrics.classification import BinaryAccuracy, BinarySpecificity, BinaryRecall, BinaryROC, BinaryAUROC
from time import perf_counter
from matplotlib import pyplot as plt

PT_CLOUD_PATH = f'/cajal/nvmescratch/users/amancu/pointMLP_data/pointMLP_pred_clouds_pts20k_ctx20k_35ksamples_bs32_2048pts_64knn_cls_run1/'
INFERENCE_BATCH_SIZE = 9

GREY = np.array([180., 180., 180., 255.])
PINK = np.array([10., 255., 10., 255.])


class PointMLPModule(pl.LightningModule):
    def __init__(self, num_cls=2, lr=1e-3, step_size=50, **kwargs):
        """
        :param n_ch: number of channels
        :param nf: number of filters
        :param ks: kernel size
        :param nc: number of iterations
        :param nd: number of CRNN/BCRNN/CNN layers in each iteration
        """
        super().__init__()
        self.save_hyperparameters()

        self.num_classes = num_cls
        self.learning_rate = lr
        self.step_size = step_size

        self.model = pointMLPElite(**kwargs)

        self.cls_criterion = nn.CrossEntropyLoss()
        self.reg_criterion = nn.MSELoss()

        self.train_accuracy = BinaryAccuracy()
        self.train_recall = BinaryRecall()
        self.train_specificity = BinarySpecificity()
        self.val_accuracy = BinaryAccuracy()
        self.val_recall = BinaryRecall()
        self.val_specificity = BinarySpecificity()
        self.test_accuracy = BinaryAccuracy()
        self.test_recall = BinaryRecall()
        self.test_specificity = BinarySpecificity()

    def forward(self, x):
        """
        X is the set of points of shape (N,3,num_points)
        """
        res = self.model(x)
        return res

    def training_step(self, batch, batch_idx):
        points, reg_target, cls_target = batch
        # print(f'pts: {points.shape}\treg_target: {reg_target.shape}\tcls_target: {cls_target.shape}')
        y, rep_coords = self.model(points)

        cls_loss = self.cls_criterion(y, cls_target.squeeze(1))

        train_loss = cls_loss + (rep_coords[cls_target.squeeze(1).bool()]-reg_target[cls_target.squeeze(1).bool()]).square().sum()
        self.log("step/train_loss", train_loss)

        # Calculate accuracy
        acc = self.train_accuracy(torch.argmax(y,dim=1), cls_target.squeeze(1))
        self.train_accuracy.update(torch.argmax(y,dim=1), cls_target.squeeze(1))
        recall = self.train_recall(torch.argmax(y,dim=1), cls_target.squeeze(1))
        self.train_recall.update(torch.argmax(y,dim=1), cls_target.squeeze(1))
        specificity = self.train_specificity(torch.argmax(y,dim=1), cls_target.squeeze(1))
        self.train_specificity.update(torch.argmax(y,dim=1), cls_target.squeeze(1))

        self.log("step/train_acc", acc)
        self.log("step/train_recall", recall)
        self.log("step/train_specificity", specificity)

        return {'loss': train_loss, 'cls_pred': y, 'reg_pred': rep_coords, 'points': points}

    def validation_step(self, batch, batch_idx):
        points, feats, reg_target, cls_target, soo_size = batch
        y, rep_coords = self.model(points)

        cls_loss = self.cls_criterion(y, cls_target.squeeze(1))

        val_loss = cls_loss + (rep_coords[cls_target.squeeze(1).bool()]-reg_target[cls_target.squeeze(1).bool()]).square().sum()
        self.log("step/val_loss", val_loss)

        # Calculate accuracy
        acc = self.val_accuracy(torch.argmax(y,dim=1), cls_target.squeeze(1))
        self.val_accuracy.update(torch.argmax(y,dim=1), cls_target.squeeze(1))
        recall = self.val_recall(torch.argmax(y,dim=1), cls_target.squeeze(1))
        self.val_recall.update(torch.argmax(y,dim=1), cls_target.squeeze(1))
        specificity = self.val_specificity(torch.argmax(y,dim=1), cls_target.squeeze(1))
        self.val_specificity.update(torch.argmax(y,dim=1), cls_target.squeeze(1))

        self.log("step/val_acc", acc)
        self.log("step/val_recall", recall)
        self.log("step/val_specificity", specificity)

        return {'val_loss': val_loss, 'cls_pred': y, 'reg_pred': rep_coords, 'points': points}

    def test_step(self, batch, batch_idx: int, dataloader_idx: int = 0):
        # TODO do timing better 
        t_start = perf_counter()
        points, feats, reg_target, cls_target = batch
        points = points.squeeze(0)

        # Do batch selection for the inference in case of too many contexts
        y_acc, rep_coords_acc = [], []
        len_points = len(points)
        runs = int(len_points / INFERENCE_BATCH_SIZE)
        # print(f'len points: {len_points}, runs: {runs}')
        for i in range(runs+1):
            start = i*INFERENCE_BATCH_SIZE
            end = (i+1)*INFERENCE_BATCH_SIZE if (i+1)*INFERENCE_BATCH_SIZE <= len_points else len_points
            # if end == len_points:
            #     print(f'start: {start} and end: {end}')
            if start == end:
                continue
            y, rep_coords = self.model(points[start:end])
            y_acc.append(y)
            rep_coords_acc.append(rep_coords)

        t_end = perf_counter()
        time = t_end - t_start

        # print(len(y_acc))
        y = torch.vstack(y_acc)
        rep_coords = torch.vstack(rep_coords_acc)
        # print(f'y shape: {y.shape}')

        # Get the max for classification
        y_argmax = torch.argmax(y,dim=1)
        cls_pred = torch.Tensor([1]).to(torch.device('cuda')) if torch.count_nonzero(y_argmax) else torch.Tensor([0]).to(torch.device('cuda'))

        cls_acc_score = self.test_accuracy(cls_pred, cls_target.squeeze(1))
        self.test_accuracy.update(cls_pred, cls_target.squeeze(1))
        recall_score = self.test_recall(cls_pred, cls_target.squeeze(1))
        self.test_recall.update(cls_pred, cls_target.squeeze(1))
        specificity_score = self.test_specificity(cls_pred, cls_target.squeeze(1))
        self.test_specificity.update(cls_pred, cls_target.squeeze(1))
        # Rebatch for only the positive samples
        # print(f'reg_target shape: {reg_target.shape}\tnonzero: {reg_target.nonzero().shape}\t')
        # reg_target_masked = reg_target.flatten()[reg_target.flatten().nonzero()].view(-1,3)
        # rep_coords_masked = rep_coords.flatten()[reg_target.flatten().nonzero()].view(-1,3)

        # print(f'acc: {cls_acc_score}, recall {recall_score}, spec {specificity_score}')

        # Check for empty positive class batch
        # if reg_target_masked.shape[0] == 0:
        #     return {'cls_acc': cls_acc_score, 'reg_nmse': None, 'reg_acc': None}

        # print(f'rep_coords_masked: {rep_coords_masked.shape}\t reg_target_masked: {reg_target_masked.shape}')

        # reg_nmse_score = nmse(rep_coords_masked, reg_target_masked)
        # reg_acc_score = regression_accuracy(rep_coords_masked, reg_target_masked, thresh=1)

        # TODO Regression metrics
        return {'cls_acc': cls_acc_score, 'reg_nmse': None, 'reg_acc': None}


    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.learning_rate, betas=(0.5, 0.999))
        lr_scheduler = {
            "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=3, threshold=0.00001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False),
            "monitor": "epoch/val_loss",
            }
        return [optimizer], [lr_scheduler]


    def training_epoch_end(self, training_step_outputs):
        losses, cls_preds, reg_preds, points = [], [], [], []

        for out in training_step_outputs:
            loss, cls_pred, reg_pred, point = out['loss'], out['cls_pred'], out['reg_pred'], out['points']
            losses.append(loss)
            cls_preds.append(cls_pred)
            reg_preds.append(reg_pred)
            points.append(point)

        losses = torch.Tensor(losses)
        epoch_train_loss = torch.sum(losses)/len(losses)
        accuracy = self.train_accuracy.compute()
        recall = self.train_recall.compute()
        specificity = self.train_specificity.compute()
        self.train_accuracy.reset()
        self.train_recall.reset()
        self.train_specificity.reset()

        self.log('epoch/train_loss', epoch_train_loss)
        self.log('epoch/train_accuracy', accuracy)
        self.log('epoch/train_recall', recall)
        self.log('epoch/train_specificity', specificity)

    def validation_epoch_end(self, validation_step_outputs):
        losses, cls_preds, reg_preds, points = [], [], [], []

        for out in validation_step_outputs:
            loss, cls_pred, reg_pred, point = out['val_loss'], out['cls_pred'], out['reg_pred'], out['points']
            losses.append(loss)
            cls_preds.append(cls_pred)
            reg_preds.append(reg_pred)
            points.append(point)

        losses = torch.Tensor(losses)
        epoch_val_loss = torch.sum(losses)/len(losses)
        accuracy = self.val_accuracy.compute()
        recall = self.val_recall.compute()
        specificity = self.val_specificity.compute()
        self.val_accuracy.reset()
        self.val_recall.reset()
        self.val_specificity.reset()

        self.log('epoch/val_loss', epoch_val_loss)
        self.log('epoch/val_accuracy', accuracy)
        self.log('epoch/val_recall', recall)
        self.log('epoch/val_specificity', specificity)
        # get a random batch and a random sample in the batch
        # ind = np.random.randint(0,len(points))
        # sample_ind = np.random.randint(0,points[0].shape[0])
        # for i, ind in enumerate(indcs):
        # for GT
        pts = points[0][0].permute(1,0)
        reg_pred = reg_preds[0][0]
        cls_pred = cls_preds[0][0]
        colors = np.full(shape=(pts.shape[0], 4,), fill_value=GREY)
        colors = np.vstack((colors,PINK[np.newaxis,:]))
        # mask = np.where(out_labels[ind].cpu().detach().numpy() == 1)[0]
        # mask = np.array([[x] for x in mask])
        # try:
        #     np.put_along_axis(colors, mask, PINK, axis=0)
        # except:
        #     print("No foreground labels in original context.")
        #     pass
        mesh2obj_file_colors(PT_CLOUD_PATH + f'epoch{self.trainer.current_epoch}_real_pred{torch.argmax(cls_pred)}.ply',
            [np.array([]), np.vstack((pts.cpu().detach().numpy(),reg_pred.cpu().detach().numpy())), np.array([])], colors)

    def test_epoch_end(self, outputs):
        accuracy = self.test_accuracy.compute()
        recall = self.test_recall.compute()
        specificity = self.test_specificity.compute()
        self.test_accuracy.reset()
        self.test_recall.reset()
        self.test_specificity.reset()
        # print(f'Mean values for:\epoch_cls_acc: {epoch_cls_acc}\epoch_reg_nmse: {epoch_reg_nmse}\epoch_reg_acc: {epoch_reg_acc}')

        print(f'Mean values for:\naccuracy: {accuracy}\nrecall: {recall}\nspecificity:{specificity}')

        return {'epoch_cls_acc': accuracy, 'epoch_cls_recall': recall,'epoch_cls_specificity': specificity} #, 'epoch_reg_nmse' : epoch_reg_nmse.item(), 'epoch_reg_acc' : epoch_reg_acc.item()}




class RealCellModule(pl.LightningModule):
    def __init__(self, num_cls=2, lr=1e-3, step_size=50, **kwargs):
        """
        :param n_ch: number of channels
        :param nf: number of filters
        :param ks: kernel size
        :param nc: number of iterations
        :param nd: number of CRNN/BCRNN/CNN layers in each iteration
        """
        super().__init__()
        self.save_hyperparameters()

        self.num_classes = num_cls
        self.learning_rate = lr
        self.step_size = step_size

        self.model = pointMLPElite(**kwargs)

        self.test_accuracy = BinaryAccuracy(threshold=0.99)
        self.test_recall = BinaryRecall(threshold=0.99)
        self.test_specificity = BinarySpecificity(threshold=0.99)
        self.test_roc = BinaryROC()
        self.test_auc = BinaryAUROC()

        self.mergers = []

        self.starter, self.ender = torch.cuda.Event(enable_timing=True), torch.cuda.Event(enable_timing=True)


    def forward(self, x):
        """
        X is the set of points of shape (N,3,num_points)
        """
        res = self.model(x)
        return res

    def training_step(self, batch, batch_idx):
        pass

    def validation_step(self, batch, batch_idx):
        pass

    def test_step(self, batch, batch_idx: int, dataloader_idx: int = 0):
        # TODO do better 
        device = torch.device('cuda')
        times_per_cell = []
        t_start = perf_counter()
        id, points, feats, reg_target, cls_target, sso_size = batch
        points, cls_target = points.squeeze(0), cls_target.squeeze(0)
        print(f'target: {cls_target.shape}')

        # Do batch selection for the inference in case of too many contexts
        y_acc, rep_coords_acc = [], []
        len_points = len(points)
        runs = int(len_points / INFERENCE_BATCH_SIZE)
        # print(f'len points: {len_points}, runs: {runs}')
        for i in range(runs+1):
            start = i*INFERENCE_BATCH_SIZE
            end = (i+1)*INFERENCE_BATCH_SIZE if (i+1)*INFERENCE_BATCH_SIZE <= len_points else len_points
            if start == end:
                continue
            y, rep_coords = self.model(points[start:end])
            y_acc.append(y)
            rep_coords_acc.append(rep_coords)

        t_end = perf_counter()
        curr_time = t_end - t_start #starter.elapsed_time(ender) * 1e-3
        size = sso_size.detach().cpu().numpy()[0]
        Mvx_s = size/curr_time * 1e-6
        times_per_cell.append(curr_time)

        print(f'Elapsed time for cell {curr_time:.3f}s for a cell of size: {size}\nTotal {Mvx_s:.3f}MVx/s')

        # print(len(y_acc))
        y = torch.vstack(y_acc)
        rep_coords = torch.vstack(rep_coords_acc)
        # print(f'y shape: {y.shape}')

        # Get the max for classification
        y_argmax = torch.argmax(y,dim=1)
        pos_pred_logits = y[torch.where(y_argmax == 1)]
        # print(pos_pred_logits)
        if torch.any(pos_pred_logits):
            pos_softmax = torch.nn.Softmax(dim=1)(pos_pred_logits)
            # print(pos_softmax)
            cls_pred = torch.tensor([pos_softmax.max()]).to(device)
            # print(cls_pred)
        else:
            cls_pred = torch.Tensor([0]).to(device)

        cls_acc_score = self.test_accuracy(cls_pred, cls_target)
        cls_recall_score = self.test_recall(cls_pred, cls_target)
        cls_specificity_score = self.test_specificity(cls_pred, cls_target)
        self.test_accuracy.update(cls_pred, cls_target)
        self.test_recall.update(cls_pred, cls_target)
        self.test_specificity.update(cls_pred, cls_target)
        self.test_roc.update(cls_pred, cls_target)
        self.test_auc.update(cls_pred, cls_target)

        print(f'cls pred: {cls_pred}, target: {cls_target}, acc: {cls_acc_score}')

        if torch.any(cls_pred>0.99):
            self.mergers.append(id.squeeze().detach().cpu().numpy())

        return {'cls_acc': cls_acc_score, 'MVx_s': Mvx_s, 'recall': cls_recall_score, 'specificity': cls_specificity_score, 'size': size}


    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.learning_rate, betas=(0.5, 0.999))
        lr_scheduler = {
            "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, threshold=0.00001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False),
            "monitor": "epoch/val_loss",
            }
        return [optimizer], [lr_scheduler]


    def training_epoch_end(self, training_step_outputs):
        pass

    def validation_epoch_end(self, validation_step_outputs):
        pass

    def test_epoch_end(self, outputs):
        batch_losses_cls_acc = [x["cls_acc"] for x in outputs]
        batch_MVxs = [x["MVx_s"] for x in outputs]
        batch_recall = [x["recall"] for x in outputs]
        batch_specificity = [x["specificity"] for x in outputs]
        batch_size = [x["size"] for x in outputs]
        # batch_losses_reg_nmse = [torch.from_numpy(x["reg_nmse"]) for x in outputs]  
        # batch_losses_reg_acc = [x["reg_acc"] for x in outputs]

        epoch_cls_acc = self.test_accuracy.compute()
        epoch_cls_recall = self.test_recall.compute()
        epoch_cls_specificity = self.test_specificity.compute()
        epoch_cls_roc = self.test_roc.compute()
        epoch_cls_auroc = self.test_auc.compute()
        # epoch_reg_nmse = torch.FloatTensor(batch_losses_reg_nmse).mean()
        # epoch_reg_acc = torch.FloatTensor(batch_losses_reg_acc).mean()
        # print(f'Mean values for:\epoch_cls_acc: {epoch_cls_acc}\epoch_reg_nmse: {epoch_reg_nmse}\epoch_reg_acc: {epoch_reg_acc}')

        batch_acc = torch.DoubleTensor(batch_losses_cls_acc).detach().cpu().numpy()
        batch_size = torch.DoubleTensor(batch_size).detach().cpu().numpy()
        batch_recall = torch.DoubleTensor(batch_recall).detach().cpu().numpy()
        batch_specificity = torch.DoubleTensor(batch_specificity).detach().cpu().numpy()
        batch_MVxs  = torch.DoubleTensor(batch_MVxs).detach().cpu().numpy()
        fpr, tpr, thresholds = epoch_cls_roc[0].detach().cpu().numpy(), epoch_cls_roc[1].detach().cpu().numpy(), epoch_cls_roc[2].detach().cpu().numpy()
        batch_auroc_score = epoch_cls_auroc.detach().cpu().numpy()

        li = list(zip(batch_size, batch_acc))
        plt.scatter(*zip(*li), marker="o")
        plt.title('Accuracy per Cell Size')
        plt.xlabel('Cell sizes (Vx scaled for 1e9)')
        plt.ylabel('Cell prediction accuracy')
        plt.savefig('realCells_size_acc_cube.png')
        plt.close()

        li = list(zip(batch_size, batch_MVxs))
        plt.scatter(*zip(*li), marker="o")
        plt.title('Model Speed per Cell Size')
        plt.xlabel('Cell sizes (Vx scaled for 1e9)')
        plt.ylabel('Cell prediction speed (MVx/s)')
        plt.savefig('realCells_size_speed_cube.png')
        plt.close()

        li = list(zip(fpr, tpr))
        plt.scatter(*zip(*li), marker="o")
        plt.title(f'ROC with AUC: {batch_auroc_score}')
        plt.xlabel('FPR')
        plt.ylabel('TPR (Recall)')
        ind = np.random.choice(len(li),30)
        for i in range(30):
            plt.annotate(f'{thresholds[ind[i]]:.3f}', li[ind[i]])       
        plt.savefig('ROC_curve_cube.png')
        plt.close()
        print(f'Thresholds: {thresholds}')

        mrgs = np.array(self.mergers)
        print(f'Mergers with ids: {mrgs}')
        np.save(f'/cajal/nvmescratch/users/amancu/merge-error-detection/mergers.npy', mrgs)

        print(f'accs: {epoch_cls_acc}\nrecall: {epoch_cls_recall}\nspecificities: {epoch_cls_specificity}, mean speed {batch_MVxs.mean():.3f} Mvx/s')

        return {'epoch_cls_acc': epoch_cls_acc}