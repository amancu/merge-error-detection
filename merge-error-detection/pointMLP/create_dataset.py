import numpy as np
import os
import torch
import timeit
import argparse
import gc
import multiprocessing as mp
import threading as th
import networkx as nx


from tqdm import tqdm
from scipy.spatial import cKDTree
from syconn import global_params
from syconn.handler.config import initialize_logging
from syconn.handler.basics import write_obj2pkl, load_pkl2obj
from syconn.reps.segmentation import SegmentationDataset
from syconn.reps.super_segmentation import SuperSegmentationObject, SuperSegmentationDataset
from syconn.proc.meshes import calc_contact_syn_mesh, mesh2obj_file_colors, merge_meshes, gen_mesh_voxelmask
from syconn.proc.ssd_proc import merge_ssv
from syconn.backend.storage import AttributeDict, MeshStorage, VoxelStorage, VoxelStorageDyn, VoxelStorageLazyLoading
from morphx.classes.hybridmesh import HybridCloud


def get_cell_embeddings_cmpt_preds(ssd: SuperSegmentationDataset, ssv_id: int) -> np.ndarray:
    """Gets the cell embedding and compartment predictions per node per node of one cell
    comp: 0 (dendrite), 1 (axon), 2 (soma), 3 (axon - en passant), 4 (axon - bouton)

    Args:
        ssd (SuperSegmentationDataset): set of agglomerated supervoxels
        ssv_id (int): ID of the cell in the dataset

    Returns:
        np.ndarray: cell embeddings (num_skel_nodes,10)
        np.ndarray: cell compartment predictions (num_skel_nodes,)
    """
    ssv = ssd.get_super_segmentation_object(ssv_id)
    ssv.load_skeleton()
    embedding = ssv.skeleton["latent_morph_wholecell"]  # embeddings of the skeleton nodes
    comp = ssv.skeleton["axoness_avg10000"]             # compartment prediction of each node

    return embedding, comp


