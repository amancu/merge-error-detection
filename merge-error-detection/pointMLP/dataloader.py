import glob
import numpy as np
import torch
import pytorch_lightning as pl

from syconn.reps.super_segmentation import SuperSegmentationDataset
from syconn import global_params
from sklearn import model_selection
from torch.utils.data import Dataset, DataLoader
from typing import List
from utils import pts_loader_semseg_train_mlp, pts_loader_semseg_infer_mlp
from utils import mesh2obj_file_colors


PT_CLOUD_PATH = f'/cajal/nvmescratch/users/amancu/pointMLP_data/pointMLP_input_clouds/'
PT_CLOUD_PATH2 = f'/cajal/nvmescratch/users/amancu/pointMLP_data/pointMLP_input_clouds_original/'
GREY = np.array([180., 180., 180., 255.])
PINK = np.array([10., 255., 10., 255.])
COLOR = np.array([255., 10., 10., 255.])

class PointCloudLoader(Dataset):
    def __init__(self, split = None, fnames = None, transform: List = None):

        self.split = split
        self.fnames = fnames

        if transform is None: 
            print(f'The dataloader does not have any transforms! (Normalization and Scale)')
            raise NotImplementedError(f'No Transforms for the clouds is not implemented. The training is destined to fail!')

        self.transform = transform

    def __getitem__(self, item):
        if self.split == 'train':
            item = np.random.randint(0, self.__len__())
        
        if self.split != 'test':
            hc, points, feats, reg_target, init_reg = pts_loader_semseg_train_mlp(self.fnames[item],batchsize=1,npoints=20000, ctx_size=int(20e3), transform=self.transform)

            # # Save original cloud
            # colors = np.full(shape=(hc.vertices.shape[0], 4,), fill_value=GREY)
            # tree = cKDTree(hc.vertices)
            # inds = tree.query_ball_point(init_reg, int(10e3))
            # mask = np.array([[x] for x in inds])
            # np.put_along_axis(colors, mask, COLOR, axis=0)
            # colors = np.vstack((colors,PINK[np.newaxis,:]))
            # mesh2obj_file_colors(PT_CLOUD_PATH2 + f'sample{item}_real.ply',
            #     [np.array([]), np.vstack((hc.vertices,init_reg[np.newaxis,:])), np.array([])], colors)

            # if np.any(init_reg):
            #     # Save input cloud
            #     colors = np.full(shape=(points.shape[0], 4,), fill_value=GREY)
            #     colors = np.vstack((colors,PINK[np.newaxis,:]))
            #     mesh2obj_file_colors(PT_CLOUD_PATH + f'sample{item}_real.ply',
            #         [np.array([]), np.vstack((points,reg_target[np.newaxis,:])), np.array([])], colors)

            # Transform into tensors
            points = torch.from_numpy(points).permute(1,0).contiguous().float()
            if not np.any(init_reg):
                # print(f'True negative')
                reg_target = torch.from_numpy(init_reg)
                cls_target = torch.Tensor([0]).long()
            else:
                # print(f'True positive')
                reg_target = torch.from_numpy(reg_target)
                cls_target = torch.Tensor([1]).long()

            # print(f'points: {points.shape}\treg: {reg_target.shape}\tcls: {cls_target.shape}')
            return points, reg_target, cls_target
        
        else:
            hc, points, feats, src_node, reg_target, init_reg, soo_size = pts_loader_semseg_infer_mlp(self.fnames[item],npoints=20000, ctx_size=int(20e3), transform=self.transform)

            # if np.any(init_reg):
            #     # Save input cloud
            #     colors = np.full(shape=(points.shape[0], 4,), fill_value=GREY)
            #     colors = np.vstack((colors,PINK[np.newaxis,:]))
            #     mesh2obj_file_colors(PT_CLOUD_PATH + f'sample{item}_real.ply',
            #         [np.array([]), np.vstack((points,reg_target[np.newaxis,:])), np.array([])], colors)

            # Transform into tensors
            # print(f'points {points.shape}')
            # print(f'verts: {points[1]} \n\n {points[3]}')
            points = torch.from_numpy(points).permute(0,2,1).contiguous().float()
            # print(f'verts: {points[1]} \n\n {points[3]}')
            if not np.any(init_reg):
                # print(f'True negative')
                reg_target = torch.from_numpy(init_reg)
                cls_target = torch.Tensor([0]).long()
            else:
                # print(f'True positive')
                reg_target = torch.from_numpy(reg_target)
                cls_target = torch.Tensor([1]).long()

            # print(f'points: {points.shape}\treg: {reg_target.shape}\tcls: {cls_target.shape}')
            return points, feats, reg_target, cls_target

    def __len__(self):
        return len(self.fnames)


class PointCloudModule(pl.LightningDataModule):
    def __init__(self, batch_size=4, transforms=None, root=None, num_workers=4, shuffle=False, limit_num_samples=None):

        self.fpath = root
        if self.fpath is None:
            self.fpath = f'/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only/'

        self.fnames = glob.glob(self.fpath + '*.pkl')

        if self.fnames == [] or self.fnames is None:
            raise BaseException(f'There has been no found at this location: {self.fpath}')

        print(f'Dataset contains {len(self.fnames)} samples')

        self.batch_size = batch_size
        self.num_workers = num_workers
        self.transforms = transforms
        self.root = root
        self.shuffle = shuffle
        self.limit_num_samples = limit_num_samples

        # only use 90% for train-val, 10% is always test
        # from which train is 80% and val is 20%
        train_test_split = int(0.9 * len(self.fnames))
        self.train_split, self.val_split = model_selection.train_test_split(self.fnames[:train_test_split], test_size=0.2, shuffle=self.shuffle)
        self.test_split = self.fnames[train_test_split:]

        ##### NOTE FOR TESTING PURPOSES TODO
        # self.train_split, self.val_split, self.test_split = self.fnames[:8], self.fnames[:8], self.fnames[:8]

        # Limit all predefined paths to the number of limited samples
        if self.limit_num_samples is not None:
            self.train_split = self.train_split[:self.limit_num_samples]
            self.val_split = self.val_split[:self.limit_num_samples]
            self.test_split = self.test_split[:self.limit_num_samples]

        print(f'Processed split:\nTrain\t{len(self.train_split)}\nVal\t{len(self.val_split)}\nTest\t{len(self.test_split)}')

        self.train_dataset = PointCloudLoader('train', self.train_split, transform=self.transforms['train'])

        self.validation_dataset = PointCloudLoader('val', self.val_split, transform=self.transforms['val'])

        self.test_dataset = PointCloudLoader('test', self.test_split, transform=self.transforms['test'])


    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size,
                          num_workers=self.num_workers, pin_memory=True)

    def val_dataloader(self):
        return DataLoader(self.validation_dataset, batch_size=self.batch_size,
                          num_workers=self.num_workers, pin_memory=True)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size,
                          num_workers=self.num_workers, pin_memory=True)


# Have a dataloader + module for inference on real cells
class RealCellLoader(Dataset):
    def __init__(self, ssv_ids_tp, ssv_ids_tn, transform, ssd) -> None:
        super().__init__()
        self.ssv_ids_tp = ssv_ids_tp
        self.ssv_ids_tn = ssv_ids_tn
        self.transform = transform
        self.ssd = ssd

    def __getitem__(self, item):
        if item < len(self.ssv_ids_tp):
            id = int(self.ssv_ids_tp[item])
            hc, points, feats, src_node, reg_target, init_reg, sso_size = pts_loader_semseg_infer_mlp(id,npoints=20000, ctx_size=int(20e3), transform=self.transform, ssd=self.ssd)
            reg_target = torch.from_numpy(reg_target)
            cls_target = torch.Tensor([1]).long()
        else:
            id = int(self.ssv_ids_tn[item-len(self.ssv_ids_tp)])
            hc, points, feats, src_node, reg_target, init_reg, sso_size = pts_loader_semseg_infer_mlp(id,npoints=20000, ctx_size=int(20e3), transform=self.transform, ssd=self.ssd)
            reg_target = torch.from_numpy(init_reg)
            cls_target = torch.Tensor([0]).long()

        # Transform into tensors
        points = torch.from_numpy(points).permute(0,2,1).contiguous().float()

        return id, points, feats, reg_target, cls_target, sso_size

    def __len__(self):
        return len(self.ssv_ids_tp) + len(self.ssv_ids_tn)

class RealCellDataModule(pl.LightningDataModule):
    def __init__(self, batch_size=1, transforms=None, ssv_ids_tp=None, ssv_ids_tn=None, ssd=None, num_workers=4, shuffle=False, limit_num_samples=None):

        self.ssd = ssd
        self.ssv_ids_tp = ssv_ids_tp
        self.ssv_ids_tn = ssv_ids_tn
        if self.ssv_ids_tp is None or self.ssv_ids_tn is None:
            if self.ssd is None:
                global_params.wd = '/cajal/nvmescratch/projects/from_ssdscratch/songbird/j0251/j0251_72_seg_20210127_agglo2/'
                self.ssd = SuperSegmentationDataset()
            self.ssv_ids_tp = ssd.ssv_ids

        len_tp, len_tn = len(self.ssv_ids_tp), len(self.ssv_ids_tn)
        print(f'Dataset contains {len_tp + len_tn} samples, from which {len_tp} positives and {len_tn} negatives')

        self.batch_size = batch_size
        self.num_workers = num_workers
        self.transforms = transforms
        self.shuffle = shuffle
        self.limit_num_samples = limit_num_samples

        self.test_dataset = RealCellLoader(self.ssv_ids_tp, self.ssv_ids_tn, transform=self.transforms['test'], ssd=self.ssd)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size,
                          num_workers=self.num_workers, pin_memory=True)