import torch
import pytorch_lightning as pl
import numpy as np
import os
import random

from utils import Center, Normalization
from dataloader import RealCellDataModule
from module import RealCellModule
from syconn import global_params
from syconn.reps.super_segmentation import SuperSegmentationDataset

def set_seed(seed=None):
    if seed is None:
        return
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = ("%s" % seed)
    np.random.seed(0)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

if __name__ == '__main__':
    # Define datamodule specific parameters (config)
    BS = 1
    SCALE_NORM = 5000
    TRANSFORM = {
        'train': [Center(), Normalization(SCALE_NORM)],
        'val': [Center(), Normalization(SCALE_NORM)],
        'test': [Center(), Normalization(SCALE_NORM)],
    }

    # Only this needed for training dataloader actually
    ROOT = f'/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only/'

    LIMIT_NUM_SAMPLES = None
    NUM_WORKERS = 8
    SHUFFLE = True
    INFERENCE_BATCH_SIZE = 9

    # For agglo2
    SSV_IDS_TP = [67855360, 2303737902, 1663419192, 1303277836, 11241738, 349315872, 11241738, 8772517, 26317341, 412607305, 54119018, 1245380836, 8772517, 1245380836, 656382718, 15320718, 64210280, 41972704, 731216132, 870732015, 777573524, 899643648, 52230331, 372794785, 19063192, 3341042, 2834364, 11541121, 63336405, 1179179749, 13429878, 336759996, 17513412, 71968375, 243809477, 446586134, 456606633, 1075125902, 1415427719, 1477208424, 1639126257, 2211357026, 147885333, 172650885, 249947186, 497732778, 570446074, 795288710, 1842544981, 2320304436, 2366088752, 1423628568, 236050030, 4656121, 643961774, 36709188, 525199305, 3171878, 25544419, 19063192, 819950340, 63264864]
    SSV_IDS_TN = [662520364, 659452724, 336017613, 1476062410, 1259010748, 48281984, 1692434078, 1032208935, 666941314, 1355133834, 929741874, 968947981, 1176548832, 1377539166, 1384387277, 1155597132, 1572357060, 657494197, 1672731062, 872518899, 1383107507, 1476062410, 1165279248, 1291875338, 32528127, 68697695, 1413741887, 1741528255, 1944944917, 1952501443, 2666915422, 2706224070, 2913251907, 779565044, 1637910727, 1818421264, 2111955797, 2200894389, 2250462919, 4932317, 232539820, 418107599, 1187179229, 1435133458, 1484426888, 1699420320, 1940725922, 2027710041, 2048190794, 2330526191, 2713439002, 2786992211, 4352489, 14173320, 50141702, 50646070, 159121091, 322591071, 1132860291, 2826603861]
    # For Rag_Flat_v3
    # SSV_IDS_TP = [132138796, 224145260, 351931151, 351931151, 205268369, 81874455, 81874455, 316966179, 271546135, 111149122, 2972211, 9059655, 129073203, 362261505, 410345250, 23256852]
    # SSV_IDS_TN = [163615279, 198419101, 211640326, 151114073, 128112837, 140383377, 200743709, 200920352]
    # global_params.wd = '/cajal/nvmescratch/projects/from_ssdscratch/songbird/j0251/rag_flat_Jan2019_v3/'
    global_params.wd = '/cajal/nvmescratch/projects/from_ssdscratch/songbird/j0251/j0251_72_seg_20210127_agglo2/'
    SSD = SuperSegmentationDataset()

    datamodule = RealCellDataModule(batch_size = BS, transforms = TRANSFORM, ssv_ids_tp=SSV_IDS_TP, ssv_ids_tn=SSV_IDS_TN, ssd=SSD, num_workers = NUM_WORKERS, 
                                    shuffle=SHUFFLE, limit_num_samples = LIMIT_NUM_SAMPLES)

    test_loader = datamodule.test_dataloader()

    # Define model
    hyperparameters = {
        'lr' : 1e-3,
        'num_classes' : 2,
        'step_size': 10}

    model = RealCellModule(num_cls=2, **hyperparameters)

    # Define triaing parameters
    NUM_EPOCHS = 150
    ACCELERATOR = "gpu"
    DEVICES = "auto"

    # CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run0/checkpoints/epoch=22-step=63871.ckpt'
    CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run1/checkpoints/epoch=32-step=91641.ckpt'
    # model.load_from_checkpoint(CHKPT_PATH)

    print(f'Inference for model pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run0 on REAL mergers')

    # Define Trainer and start the training 
    trainer = pl.Trainer(max_epochs=NUM_EPOCHS,
                        accelerator=ACCELERATOR, #devices=DEVICES,
                        log_every_n_steps=2,
                        # strategy=DDPStrategy(find_unused_parameters=False)
                        )

    trainer.test(model, dataloaders=test_loader, ckpt_path=CHKPT_PATH, verbose=True)