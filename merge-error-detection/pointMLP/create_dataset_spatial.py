import os
import numpy as np
import multiprocessing as mp
import open3d as o3d
import pickle as pkl

from tqdm import tqdm
from scipy.spatial import cKDTree
from syconn import global_params
from syconn.reps.segmentation import SegmentationDataset
from syconn.reps.super_segmentation import SuperSegmentationObject, SuperSegmentationDataset
from syconn.proc.meshes import merge_meshes, gen_mesh_voxelmask
from syconn.backend.storage import VoxelStorage
from morphx.classes.hybridmesh import HybridCloud
from morphx.processing.objects import context_splitting_graph_many
from utils import mesh2obj_file_colors

# Paths
TARGET_PATH = '/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only_optimized/'
FILTERED_CS_IDS_PATH = '/cajal/nvmescratch/users/amancu/pointMLP_data/filtered_cs_ids[50k-100k].npy'
LOOKUP_CELLPAIR2CS_PATH = '/cajal/nvmescratch/users/amancu/pointMLP_data/lookup_cellpair2cs[50k-100k]_filteredSSVs.pkl'

# Dataset generation parameters
OFFSET = 0
NR_SAMPLES = int(25e3)
DATASET_LIMIT = 10 # len(NR_SAMPLES)
NR_CORES = 60

# For context extraction
AREA_OF_INTEREST = int(20e3)

GREY = np.array([180., 180., 180., 255.])
PINK = np.array([10., 255., 10., 255.])

def merge_ssv_and_get_source_node_idcs(cell_obj1, cell_obj2, cs_coord_list):
    """
        Merge two cell objects into one"

        Notes:
            Skeleton is in voxel coordinates

        Parameters
        ----------
        cell_obj1, cell_obj2 : SuperSegmentationObject
            Two cells to be merged.
        cs_coord_list : List of locations representing each area of contact site
    """

    merged_cell = SuperSegmentationObject(ssv_id=-1, working_dir=None, version='tmp')
    for mesh_type in [
        'sv']:  # , 'syn_ssv', 'vc', 'mi']:                                     # 'sj' fails for current v3 dataset (Not Found)
        mesh1 = cell_obj1.load_mesh(mesh_type)
        mesh2 = cell_obj2.load_mesh(mesh_type)
        ind_lst = [mesh1[0], mesh2[0]]
        vert_lst = [mesh1[1], mesh2[1]]

        merged_cell._meshes[mesh_type] = merge_meshes(ind_lst, vert_lst)
        merged_cell._meshes[mesh_type] += ([None, None],)  # add normals

    # m = merged_cell._meshes['sv']
    # print(f'meshes: {m}')
    # remove 

    # merge skeletons
    merged_cell.skeleton = {}
    cell_obj1.load_skeleton()
    cell_obj2.load_skeleton()
    edge_idc_offset = len(cell_obj1.skeleton['nodes'])
    merged_cell.skeleton['edges'] = np.concatenate([cell_obj1.skeleton['edges'],
                                                    cell_obj2.skeleton['edges'] +
                                                    edge_idc_offset])  # additional offset

    # merge UNSCALED skeleton nodes
    merged_cell.skeleton['nodes'] = np.concatenate([cell_obj1.skeleton['nodes'],
                                                    cell_obj2.skeleton['nodes']])

    # Find the all 2 nodes that are the nearest to the contact site areas
    scaled_skeleton1 = cell_obj1.skeleton['nodes'] * merged_cell.scaling
    scaled_skeleton2 = cell_obj2.skeleton['nodes'] * merged_cell.scaling
    node_pairs = []

    node_tree1 = cKDTree(data=scaled_skeleton1)
    node_tree2 = cKDTree(data=scaled_skeleton2)

    # find first neighboring node from each skeleton
    for cs_coord in cs_coord_list:
        _, idcs1 = node_tree1.query(cs_coord, k=1, workers=2)
        _, idcs2 = node_tree2.query(cs_coord, k=1, workers=2)
        try:
            node_pairs.append([idcs1, idcs2 + edge_idc_offset])
        except Exception as e:
            print(f'Exception: {e}')
            continue  # if no neighbor was found in either nn searches

    # Delete the vertices right next to the merge spots
    merged_cell_verts = merged_cell.mesh[1].reshape(-1, 3)
    verts_tree = cKDTree(data=merged_cell_verts)
    idcs = verts_tree.query_ball_point(cs_coord_list, r=150, workers=2)
    idcs = np.concatenate((idcs)).astype(np.uint)
    try:
        merged_cell._meshes['sv'] = (merged_cell._meshes['sv'][0], np.delete(merged_cell_verts, idcs, axis=0).reshape(-1), [None,None])
    except Exception as e:
        print(f'Exception in merge optimization: {e}')

    # add remaining edges of the merge spot
    merged_cell.skeleton['edges'] = np.concatenate([merged_cell.skeleton['edges'], np.unique(node_pairs,
                                                                                             axis=0)])  # node_pairs should be unique, as some node connecttions repeat themselves
    merged_cell.skeleton['diameters'] = np.concatenate([cell_obj1.skeleton['diameters'],
                                                        cell_obj2.skeleton['diameters']])

    return merged_cell, set(np.unique(node_pairs))



def create_HCs(cell_pair2cs_ids, cell_pairs, slice, cs_dataset):
    print(f'in HCs')
    # process every cell pair
    for cellpair in tqdm(cell_pairs[slice], desc='Sample gen'):
        cell1 = cellpair[0]
        cell2 = cellpair[1]
        # if cells are same, skip
        if cell1 == cell2:
            print(f'Dict cells are the same for: {cell1}')
            continue

        # Check if the cell pair exists already
        # truth_array = [os.path.exists(TARGERT_PATH + f'sso_{cell1}_{cell2}.pkl') for radius in radii]
        # if np.all(truth_array):
        #     continue
        if os.path.exists(TARGET_PATH + f'sso_{cell1}_{cell2}.pkl'):
            continue

        # get partner cells, merge them, and get contact site mesh
        fstObj = ssd.get_super_segmentation_object(cell1)
        sndObj = ssd.get_super_segmentation_object(cell2)

        # cs coords for skeleton nodes
        cs_coord_list = []
        ind = None
        for cs_id in cell_pair2cs_ids[(cell1, cell2)]:
            cs = cs_dataset.get_segmentation_object(cs_id)
            # list of cs meshes [(indices, vertices, normals)]
            voxel_dc = VoxelStorage(cs.voxel_path, read_only=True, disable_locking=True)
            voxel_iter = voxel_dc.iter_voxelmask_offset(cs.id, overlap=1)
            cs_mesh = gen_mesh_voxelmask(voxel_iter, cs.scaling, overlap=1, compute_connected_components=True)
            # Get the largest mesh contact site
            meshes = [mesh[1].reshape(-1, 3) for mesh in cs_mesh]
            sizes = [len(mesh) for mesh in meshes]
            ind = sizes.index(max(sizes))
            for mesh in cs_mesh:
                area_mesh = mesh[1].reshape(-1, 3)
                pcd = o3d.geometry.PointCloud()
                pcd.points = o3d.utility.Vector3dVector(area_mesh)
                voxel_size = 300
                _, idcs, _ = pcd.voxel_down_sample_and_trace(voxel_size, pcd.get_min_bound(), pcd.get_max_bound())
                rep_coords = area_mesh[np.max(idcs, axis=1)]
                # choose the mean coords on each axis as the representative coordinate for the contact site
                # rep_coord = np.mean(rep_coords, axis=0)
                # print(f'rep coord: {rep_coord}')
                cs_coord_list.extend(rep_coords)
        
        # Choose one rep_coord for biggest contact site mesh area
        representative_coord_merger = cs_coord_list[ind]

        merged_cell, source_node_idcs = merge_ssv_and_get_source_node_idcs(fstObj, sndObj, cs_coord_list)
        merged_cell_verts = merged_cell.mesh[1].reshape(-1, 3)
        merged_cell_nodes = merged_cell.skeleton['nodes'] * merged_cell.scaling
        features = np.zeros(shape=(len(merged_cell_verts),), dtype=np.int32)

        source_node_labels = np.zeros(shape=(len(merged_cell_nodes),), dtype=np.int32)
        node_kdt = cKDTree(merged_cell_nodes)

        hc = HybridCloud(vertices=merged_cell_verts, labels=features ,features=features, predictions={'1':representative_coord_merger},
                    nodes=merged_cell_nodes, edges=merged_cell.skeleton['edges'])

        node_tree = cKDTree(data=merged_cell_nodes)
        src_node = node_tree.query(representative_coord_merger, k=1)[1]

        # set first the context extraction area
        idcs = context_splitting_graph_many(hc, [src_node], AREA_OF_INTEREST)[0] #np.unique(node_kdt.query_ball_point(representative_coord_merger, r=AREA_OF_INTEREST))
        source_node_labels[idcs] = int(1)

        # Check if merge is inside future context for each point
        for ind in idcs:
            dist = np.linalg.norm(merged_cell_nodes[ind] - representative_coord_merger)
            if dist > int(20e3):
                print(f'Source node outside the future context')
                source_node_labels[ind] = int(0)


        has_src_nodes = np.any(np.where(source_node_labels==1))
        print(f'source nodes? {has_src_nodes}')
        if not has_src_nodes:
            print(f'Could not find any source nodes for cells {cell1} and {cell2}. Aborting...')
            raise RuntimeError(f'No source nodes found')

        print(f'src labels: {source_node_labels.shape}, nodes {merged_cell_nodes.shape}')

        hc = HybridCloud(vertices=merged_cell_verts, labels=features ,features=features, predictions={'1':representative_coord_merger},
                    nodes=merged_cell_nodes, edges=merged_cell.skeleton['edges'], node_labels=source_node_labels)

        if hc.save2pkl(TARGET_PATH + f'sso_{cell1}_{cell2}.pkl'):
            print(f'Hybridcloud for cells {cell1} and {cell2} could not be written!')

        # Save cloud
        # colors = np.full(shape=(hc.vertices.shape[0], 4,), fill_value=GREY)
        # colors = np.vstack((colors,PINK[np.newaxis,:]))
        # mesh2obj_file_colors(TARGET_PATH + f'sso_{cell1}_{cell2}_150.ply',
        #     [np.array([]), np.vstack((hc.vertices, representative_coord_merger[np.newaxis,:])), np.array([])], colors)


def create_lookup_table(inp):
    """
    inp = (filtered_contact_sites_ids, cs_dataset, slice)
    Loop through all contact_sites ids and if two corresponding cells are found,
    store the cell_id and corresponding cs_id into dictionary

    Returns
    -------
    cell_pair2cs_ids : dict
    cell_pairs : list
    """
        
    filtered_contact_sites_ids, cs_dataset, slice = inp[0], inp[1], inp[2]
    
    cell_pair2cs_ids = {}
    cell_pairs = []
    ssd = SuperSegmentationDataset()
    sizes = ssd.load_numpy_data('size')
    # Range has approx 5mil cells
    low = np.percentile(sizes,30)           # Too small means unseeable mergers
    high = np.percentile(sizes,80)          # Too big means a lot of processing
    valid_ids = ssd.ssv_ids[(sizes > low) & (sizes < high)]

    # NOTE only works with agglo2, because SV equal SSV
    for cs_id in tqdm(filtered_contact_sites_ids[slice]):
        sv_partner = cs_dataset.get_segmentation_object(cs_id).cs_partner
        if sv_partner[0] in valid_ids and sv_partner[1] in valid_ids:
            dict = ssd.sv2ssv_ids(sv_partner)
            c1, c2 = dict[sv_partner[0]], dict[sv_partner[1]]

            if c1 == c2:
                continue
            if c1 < c2:
                if (c1, c2) in cell_pair2cs_ids:
                    cell_pair2cs_ids[(c1, c2)].append(cs_id)
                else:
                    cell_pair2cs_ids[(c1, c2)] = [cs_id]
                    cell_pairs.append((c1, c2))
            else:
                if (c2, c1) in cell_pair2cs_ids:
                    cell_pair2cs_ids[c2, c1].append(cs_id)
                else:
                    cell_pair2cs_ids[(c2, c1)] = [cs_id]
                    cell_pairs.append((c2, c1))

    print(f"len cell_pairs2cs_ids: {len(cell_pair2cs_ids)}")
    return (cell_pair2cs_ids, cell_pairs)


if __name__ == '__main__':
    # setup datasets
    global_params.wd = '/cajal/nvmescratch/projects/from_ssdscratch/songbird/j0251/j0251_72_seg_20210127_agglo2/'
    cs_dataset = SegmentationDataset(obj_type='cs')
    ssd = SuperSegmentationDataset()

    # skip small and very large CS clusters. Keep 50.000 < size < 100.000 -> 4.389.936
    if not os.path.exists(FILTERED_CS_IDS_PATH):
        # mask to get the filtered contact sites by size
        mask = (cs_dataset.sizes > int(5e4)) & (cs_dataset.sizes < int(1e5))
        filtered_cs_ids = cs_dataset.ids[mask]
        print(f' Number of filtered cs ids: {len(filtered_cs_ids)}')
        np.save(FILTERED_CS_IDS_PATH, filtered_cs_ids)
        print(f'Done writing filtered cs_ids')
    else:
        filtered_cs_ids = np.load(FILTERED_CS_IDS_PATH)
        print(f'Filtered ids gotten from file, length: {len(filtered_cs_ids)}')
    

    # create lookup table for cell ids to cs ids
    if not os.path.exists(LOOKUP_CELLPAIR2CS_PATH):
        # parallelize lookup table
        chunksize = len(filtered_cs_ids) // NR_CORES
        proc_slices = []
        for i_proc in range(NR_CORES):
            chunkstart = int(OFFSET + (i_proc * chunksize))
            # make sure to include the division remainder for the last process
            chunkend = int(OFFSET + (i_proc + 1) * chunksize) if i_proc < NR_CORES - 1 else int(OFFSET + len(filtered_cs_ids))
            proc_slices.append(np.s_[chunkstart:chunkend])
        
        print(f'proc_slices: {proc_slices}')
        
        params = [(filtered_cs_ids, cs_dataset, slice) for slice in proc_slices]
        print(f'params {params}')
        
        # Gather multiprocess results
        cell_pair2cs_ids_list, cell_pairs = [], [] # create_lookup_table(filtered_cs_ids, cs_dataset)
        with mp.Pool(processes=NR_CORES) as pool:
            for result in pool.imap(create_lookup_table, params):
                cell_pair2cs_ids_list.append(result[0])
                cell_pairs.append(result[1])
        
        print(f'Multiprocessing done. Lengths: {len(cell_pair2cs_ids_list)}, {len(cell_pairs)}')
        # Create dictionary
        cell_pair2cs_ids = {}
        for dictionary in cell_pair2cs_ids_list:
            cell_pair2cs_ids.update(dictionary)
        # Write to pickle
        with open(LOOKUP_CELLPAIR2CS_PATH, 'wb') as f:
            pkl.dump(cell_pair2cs_ids, f)
        print(f'Cell pairs written to pickle')
    else:
        try:
            cell_pair2cs_ids = None
            with open(LOOKUP_CELLPAIR2CS_PATH, 'rb') as f:
                cell_pair2cs_ids = pkl.load(f)
            cell_pairs = []
            for key in cell_pair2cs_ids.keys():
                cell_pairs.append(key)
            print(f'Cell pairs and pair2cs dict loaded')
        except Exception as e:
            print(f'Pickle file not found. Encountered exception: {e}')
            cell_pair2cs_ids, cell_pairs = create_lookup_table(filtered_cs_ids, cs_dataset)
        # Write to pickle
        with open(LOOKUP_CELLPAIR2CS_PATH, 'wb') as f:
            pkl.dump(cell_pair2cs_ids, f)
        print(f'Cell pairs written to pickle')

    print(f'cell_pair2cs_ids len {len(cell_pair2cs_ids)}')


    # NR_SAMPLES = NR_SAMPLES[:DATASET_LIMIT]
    # NR_SAMPLES = 10
    
    # setup parallelization parameters
    print(f'Using {NR_CORES} processors')
    chunksize = NR_SAMPLES // NR_CORES
    proc_slices = []

    for i_proc in range(NR_CORES):
        chunkstart = int(OFFSET + (i_proc * chunksize))
        # make sure to include the division remainder for the last process
        chunkend = int(OFFSET + (i_proc + 1) * chunksize) if i_proc < NR_CORES - 1 else int(OFFSET + NR_SAMPLES)
        proc_slices.append(np.s_[chunkstart:chunkend])

    params = [(cell_pair2cs_ids, cell_pairs, slice, cs_dataset) for slice in proc_slices]

    running_tasks = [mp.Process(target=create_HCs, args=param) for param in params]
    for running_task in running_tasks:
        running_task.start()
    for running_task in running_tasks:
        running_task.join()
