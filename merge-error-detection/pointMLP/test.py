import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor

from utils import Center, Normalization
from dataloader import PointCloudModule
from module import PointMLPModule

# Set up callbacks
lr_monitor = LearningRateMonitor(logging_interval='step')

# Define datamodule specific parameters (config)
BS = 1
SCALE_NORM = 5000
TRANSFORM = {
    'train': [Center(), Normalization(SCALE_NORM)],
    'val': [Center(), Normalization(SCALE_NORM)],
    'test': [Center(), Normalization(SCALE_NORM)],
}

# Only this needed for training dataloader actually
ROOT = f'/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only/'

LIMIT_NUM_SAMPLES = None
NUM_WORKERS = 8
SHUFFLE = True

datamodule = PointCloudModule(batch_size = BS, transforms = TRANSFORM, root = ROOT, num_workers = NUM_WORKERS, 
                                shuffle=SHUFFLE, limit_num_samples = LIMIT_NUM_SAMPLES)

test_loader = datamodule.test_dataloader()

# Define model

hyperparameters = {
    'lr' : 1e-3,
    'num_classes' : 2,
    'step_size': 10}

model = PointMLPModule(num_cls=2, **hyperparameters)

# Define triaing parameters
NUM_EPOCHS = 150
ACCELERATOR = "gpu"
DEVICES = "auto"

CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run0/checkpoints/epoch=22-step=63871.ckpt'
# CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run1/checkpoints/epoch=32-step=91641.ckpt'

print(f'Inference for model pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run0 on artificial mergers')

# Define Trainer and start the training 
trainer = pl.Trainer(max_epochs=NUM_EPOCHS,
                    accelerator=ACCELERATOR, #devices=DEVICES,
                    # auto_scale_batch_size=True,
                    callbacks=[lr_monitor],
                    log_every_n_steps=2,
                    # strategy=DDPStrategy(find_unused_parameters=False)
                    )

from time import perf_counter
t_start = perf_counter()
trainer.test(model, dataloaders=test_loader, ckpt_path=CHKPT_PATH, verbose=True)
t_end = perf_counter()
total_time = (t_end - t_start) / 60 # in mins
print(f'Total time for processing {len(test_loader)} HybridClouds is {total_time:.3f}min ')