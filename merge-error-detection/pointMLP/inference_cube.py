import torch
import pytorch_lightning as pl
import numpy as np
import os
import random

from utils import Center, Normalization
from dataloader import RealCellDataModule
from module import RealCellModule
from syconn import global_params
from syconn.reps.super_segmentation import SuperSegmentationDataset

def set_seed(seed=None):
    if seed is None:
        return
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = ("%s" % seed)
    np.random.seed(0)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

if __name__ == '__main__':
    # Define datamodule specific parameters (config)
    BS = 1
    SCALE_NORM = 5000
    TRANSFORM = {
        'train': [Center(), Normalization(SCALE_NORM)],
        'val': [Center(), Normalization(SCALE_NORM)],
        'test': [Center(), Normalization(SCALE_NORM)],
    }

    # Only this needed for training dataloader actually
    ROOT = f'/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only/'

    LIMIT_NUM_SAMPLES = None
    NUM_WORKERS = 8
    SHUFFLE = True
    INFERENCE_BATCH_SIZE = 9

    # For agglo2
    global_params.wd = '/cajal/nvmescratch/projects/from_ssdscratch/songbird/j0251/j0251_72_seg_20210127_agglo2/'
    SSD = SuperSegmentationDataset()
    ssv_ids = SSD.ssv_ids
    sizes = SSD.load_numpy_data('size')

    SSV_IDS_TP = []
    SSV_IDS_TN = ssv_ids[sizes > np.percentile(sizes, 99.5)]            # TN untiul proven guilty, keep this here for dataloader rep coords

    print(f'SSV_IDS: {len(SSV_IDS_TP)}')

    datamodule = RealCellDataModule(batch_size = BS, transforms = TRANSFORM, ssv_ids_tp=SSV_IDS_TP, ssv_ids_tn=SSV_IDS_TN, ssd=SSD, num_workers = NUM_WORKERS, 
                                    shuffle=SHUFFLE, limit_num_samples = LIMIT_NUM_SAMPLES)

    test_loader = datamodule.test_dataloader()

    # Define model
    hyperparameters = {
        'lr' : 1e-3,
        'num_classes' : 2,
        'step_size': 10}

    model = RealCellModule(num_cls=2, **hyperparameters)

    # Define triaing parameters
    NUM_EPOCHS = 150
    ACCELERATOR = "gpu"
    DEVICES = "auto"

    CHKPT_PATH = f'/cajal/nvmescratch/users/amancu/merge-error-detection/merge-error-detection/pointMLP/lightning_logs/pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run1/checkpoints/epoch=32-step=91641.ckpt'

    print(f'Inference for model pointMLP_pts20k_ctx20k_35ksamples_bs9_2048points_64knn_cls_run0 on REAL mergers')

    # Define Trainer and start the training 
    trainer = pl.Trainer(max_epochs=NUM_EPOCHS,
                        accelerator=ACCELERATOR, #devices=DEVICES,
                        log_every_n_steps=2,
                        # devices=1,
                        # strategy='ddp_spawn'
                        )

    trainer.test(model, dataloaders=test_loader, ckpt_path=CHKPT_PATH, verbose=True)