import os
import numpy as np
import multiprocessing as mp
import pickle as pkl

from typing import List, Dict
from tqdm import tqdm
from collections import Counter
from syconn import global_params
from utils import mesh2obj_file_colors
from syconn.reps.super_segmentation import SuperSegmentationDataset
from morphx.classes.hybridmesh import HybridCloud


# Paths
TARGET_PATH = '/cajal/nvmescratch/users/amancu/pointMLP_data/spatial_data_only/'
PATH_LENGHTS = f'/cajal/nvmescratch/users/arother/j0251v4_prep/combined_fullcell_ax_dict.pkl'
FILTERED_CS_IDS_PATH = '/cajal/nvmescratch/users/amancu/pointMLP_data/filtered_cs_ids[50k-100k].npy'
LOOKUP_CELLPAIR2CS_PATH = '/cajal/nvmescratch/users/amancu/pointMLP_data/lookup_cellpair2cs[50k-100k].pkl'
MESH_PATH =f'/cajal/nvmescratch/users/amancu/pointMLP_data/pointMLP_TN/'

# Dataset generation parameters
OFFSET = 0
NR_SAMPLES = 100 #int(30e3)
DATASET_LIMIT = 100 # len(NR_SAMPLES)
NR_CORES = 50

GREY = np.array([180., 180., 180., 255.])

def create_hcs(cell_ids, slice):
    for cell_id in tqdm(cell_ids[slice]):
        curr_cell = ssd.get_super_segmentation_object(cell_id)
        curr_cell.load_skeleton()
        curr_cell.load_mesh('sv')
        curr_cell.skeleton['nodes']

        # setup verts and features
        verts = curr_cell.mesh[1].reshape(-1, 3)
        features = np.zeros(shape=(len(verts),), dtype=np.int32)
        nodes = curr_cell.skeleton['nodes'] * curr_cell.scaling
        
        # Label area of interest as whole cell
        node_labels = np.ones(shape=(len(nodes),), dtype=np.int32)

        hc = HybridCloud(vertices=verts, labels=features, features=features, predictions={'1':np.array([0.,0.,0.])},
                             nodes=nodes, node_labels=node_labels,
                             edges=curr_cell.skeleton['edges'])

        colors = np.full(shape=(verts.shape[0], 4,), fill_value=GREY)
        mesh2obj_file_colors(MESH_PATH + f'sample{cell_id}_real.ply',
            [np.array([]), verts, np.array([])], colors)

        if hc.save2pkl(os.path.expanduser(TARGET_PATH + f'{cell_id}.pkl')):
                    print(f'HybridCloud not written for {cell_id}')




if __name__ == '__main__':
    global_params.wd = '/cajal/nvmescratch/projects/from_ssdscratch/songbird/j0251/j0251_72_seg_20210127_agglo2/'
    ssd = SuperSegmentationDataset(global_params.config.working_dir)

    total_path_lengths = ssd.load_numpy_data('total_edge_length')
    path_filter = (total_path_lengths > int(1e5)) & (total_path_lengths < int(1e7))
    print(f'filter: {len(np.where(path_filter==True)[0])}')
    print(len(path_filter))

    # define wanted celltypes to insert
    msn_mask = ssd.load_numpy_data('celltype_cnn_e3') == 2
    msns = ssd.ssv_ids[msn_mask & path_filter]

    lman_mask = ssd.load_numpy_data('celltype_cnn_e3') == 3
    lmans = ssd.ssv_ids[lman_mask & path_filter]

    hvc_mask = ssd.load_numpy_data('celltype_cnn_e3') == 4
    hvcs = ssd.ssv_ids[hvc_mask & path_filter]

    GPe_mask = ssd.load_numpy_data('celltype_cnn_e3') == 6
    GPi_mask = ssd.load_numpy_data('celltype_cnn_e3') == 7
    GPs = ssd.ssv_ids[(GPe_mask | GPi_mask) & path_filter]

    # define attributes of cells
    # get each size
    sizes = ssd.load_numpy_data('size')
    msn_sizes = sizes[msn_mask & path_filter]
    lman_sizes = sizes[lman_mask & path_filter]
    hvc_sizes = sizes[hvc_mask & path_filter]
    GP_sizes = sizes[(GPe_mask | GPi_mask) & path_filter]

    # get wanted MSN cell ids
    eighty = np.percentile(msn_sizes, 80)
    ninety = np.percentile(msn_sizes, 95)
    msn_ids_to_insert = msns[(msn_sizes > eighty) & (msn_sizes < ninety)]
    # print(len(msn_ids_to_insert))
    msn_to_insert = msn_ids_to_insert#np.random.choice(msn_ids_to_insert, 40)

    if len(list((Counter(msn_to_insert) - Counter(set(msn_to_insert))).keys())) != 0:
        print("MSN array has duplicate ids. Rectifying... ")
        a = list((Counter(msn_to_insert) - Counter(set(msn_to_insert))).keys())
        mask = np.invert(np.isin(msn_to_insert, a))
        msn_to_insert = msn_to_insert[mask]
        print(f"MSN has now {len(msn_to_insert)} cell ids")

    if len(list((Counter(msn_to_insert) - Counter(set(msn_to_insert))).keys())) != 0:
        raise ValueError("MSN array has duplicate ids.")

    # get wanted LMAN cell ids
    eighty = np.percentile(lman_sizes, 80)
    ninety = np.percentile(lman_sizes, 95)
    lman_ids_to_insert = lmans[(lman_sizes > eighty) & (lman_sizes < ninety)]
    # print(len(lman_ids_to_insert))
    lman_to_insert = lman_ids_to_insert#np.random.choice(lman_ids_to_insert, 30)

    if len(list((Counter(lman_to_insert) - Counter(set(lman_to_insert))).keys())) != 0:
        print("LMAN array has duplicate ids. Rectifying... ")
        a = list((Counter(lman_to_insert) - Counter(set(lman_to_insert))).keys())
        mask = np.invert(np.isin(lman_to_insert, a))
        lman_to_insert = lman_to_insert[mask]
        print(f"LMAN has now {len(lman_to_insert)} cell ids")

    if len(list((Counter(lman_to_insert) - Counter(set(lman_to_insert))).keys())) != 0:
        raise ValueError("LMAN array has duplicate ids.")
    
    # get wanted HVC cell ids
    eighty = np.percentile(hvc_sizes, 80)
    ninety = np.percentile(hvc_sizes, 95)
    hvc_ids_to_insert = hvcs[(hvc_sizes > eighty) & (hvc_sizes < ninety)]
    # print(len(hvc_ids_to_insert))
    # hvc_to_insert = np.random.choice(hvc_ids_to_insert, 500)
    hvc_to_insert = hvc_ids_to_insert #np.random.choice(hvc_ids_to_insert, 30)

    if len(list((Counter(hvc_to_insert) - Counter(set(hvc_to_insert))).keys())) != 0:
        print("HVC array has duplicate ids. Rectifying...")
        a = list((Counter(hvc_to_insert) - Counter(set(hvc_to_insert))).keys())
        mask = np.invert(np.isin(hvc_to_insert, a))
        hvc_to_insert = hvc_to_insert[mask]
        print(f"HVC now has {len(hvc_to_insert)} cell ids")
    
    if len(list((Counter(hvc_to_insert) - Counter(set(hvc_to_insert))).keys())) != 0:
        raise ValueError("HVC array has duplicate ids.")

    # get wanted GP cell ids
    eighty = np.percentile(GP_sizes, 80)
    ninety = np.percentile(GP_sizes, 95)
    GP_ids_to_insert = GPs[(GP_sizes > eighty) & (GP_sizes < ninety)]
    # print(len(GP_ids_to_insert))
    GP_to_insert = GP_ids_to_insert #np.random.choice(hvc_ids_to_insert, 30)

    if len(list((Counter(GP_to_insert) - Counter(set(GP_to_insert))).keys())) != 0:
        print("HVC array has duplicate ids. Rectifying...")
        a = list((Counter(GP_to_insert) - Counter(set(GP_to_insert))).keys())
        mask = np.invert(np.isin(GP_to_insert, a))
        GP_to_insert = GP_to_insert[mask]
        print(f"HVC now has {len(GP_to_insert)} cell ids")
    
    if len(list((Counter(GP_to_insert) - Counter(set(GP_to_insert))).keys())) != 0:
        raise ValueError("HVC array has duplicate ids.")

    print(f'hvcs: {len(hvc_to_insert)}, lmans: {len(lman_to_insert)}, msns: {len(msn_to_insert)}, GPs: {len(GP_to_insert)}')

    ids_list = np.concatenate([msn_to_insert, lman_to_insert, hvc_to_insert, GP_to_insert])
    print(f'Total generation: {len(ids_list)}')
    # print(f'Initial ids list length: {len(ids_list)}')
    # ids_list = ids_list[:DATASET_LIMIT]
    # print(f'Actual ids list legth: {len(ids_list)}')

    chunksize = len(ids_list) // NR_CORES
    proc_slices = []

    for i_proc in range(NR_CORES):
        chunkstart = int(OFFSET + (i_proc * chunksize))
        # make sure to include the division remainder for the last process
        chunkend = int(OFFSET + (i_proc + 1) * chunksize) if i_proc < NR_CORES - 1 else int(OFFSET + len(ids_list))
        proc_slices.append(np.s_[chunkstart:chunkend])

    with mp.Pool(processes=NR_CORES) as pool:
        multiple_results = [pool.apply_async(create_hcs, (ids_list, slicee)) for slicee in proc_slices]
        print([res.get() for res in multiple_results])
