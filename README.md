# Merge Error Detection

## Description
This project has the aim of detection merge errors in EM datasets. It employs point cloud neural network (NN)architectures for fast inference speeds. The main task of these networks is to do point cloud segmentation, in order to find the location of a merge error (foreground labelled) in the point cloud.

Various NN architectures are employed, among them: 
- [X] PointConv (on SyConn's branch)
- [ ] [PointMLP](https://github.com/ma-xu/pointMLP-pytorch)

## Pipeline
Every tried architecture has the following pipeline:
- Create dataset from the latest segmentation, artificially merging cells and using proofread merged cells in the current segmentation
- Build desired model in pytorch lightning
- Evaluate models on F1 score

## Authors and acknowledgment
A big "thank you" to Joergen Kornfeld, Philipp J. Schubert and Franz Rieger for allowing me to make use of the Zebra Finch Area X dataset and for constantly assisting me in this project. 
